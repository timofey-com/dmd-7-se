#!/bin/bash

# DESCRIPTION
# Updates DB access credentials in Drupal settings file (settings-template.php)

# ARGUMENTS API
# $1 - Drupal root directory
# $2 - MYSQL username, i.e. root
# $3 - MYSQL password
# $4 - MYSQL host
# $5 - MYSQL port
# $6 - MYSQL database name

# Require root
if [ "$EUID" -ne 0 ]
  then tput setaf 1; echo "Please run as root, i.e. sudo ..."; tput sgr0
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting updatedrupal-dbsettings.sh $1 $2 $3 $4 $5 $6"; tput sgr0

# Define variables
# - Website directory
web_root="/var/www/default"

# - Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
drupal_root_dir="$1"
mysql_user="$2"
mysql_pass="$3"
mysql_host="$4"
mysql_port="$5"
mysql_db="$6"

# Setup
if [ -z "$drupal_root_dir" ]; then
    read -e -p "Enter the full location of the Drupal install: " -i "/var/www/default/public_html" drupal_root_dir
fi

if [ -z "$mysql_db" ]; then
    read -e -p "Enter the mysql database name. Do not use the name 'default': " mysql_db
fi

if [ -z "$mysql_user" ]; then
    read -e -p "Enter mysql username: " -i "root" mysql_user
fi

if [ -z "$mysql_pass" ]; then
    read -e -p "Enter mysql password: " -i "root" mysql_pass
fi
if [ -z "$mysql_host" ]; then
    read -e -p "Enter mysql host: " -i "localhost" mysql_host
fi

if [ -n "$mysql_port" ]; then
    read -e -p "Enter mysql port (leave blank for default): " mysql_port
fi



# Update Drupal DB credentials in settings file
echo "
\$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => '$mysql_db',
      'username' => '$mysql_user',
      'password' => '$mysql_pass',
      'host' => '$mysql_host',
      'port' => '$mysql_port',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);
" >> "$drupal_root_dir"/sites/default/settings.php