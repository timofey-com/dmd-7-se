#!/bin/bash

# README
# Remove domain from Virtual Host; i.e. remove host

# API
# $1 - Name of host (i.e. domain) to add

#Update Status
echo "Starting remove-host.sh $1"

# Require root
if [ "$EUID" -ne 0 ]
  then tput setaf 1; echo "Please run as root, i.e. sudo ..."; tput sgr0
  exit
fi

# Load Variables
host_name=$1

# Disable the site/host
# - Ubuntu 12.04 doesn't prefix with .conf, Ubuntu 14.04 does
a2dissite ${host_name}
a2dissite ${host_name}.conf

# Remove a domain-specific Virtual Host
if [ $host_name == '000-default' ] || [ $host_name == 'default' ] ; then
    rm -rf /var/www/default
    rm -f /var/www/index.html
else
    rm -f /etc/apache2/sites-available/$host_name.conf
    rm -rf /var/www/$host_name
fi

# Reload Apache2
service apache2 reload
