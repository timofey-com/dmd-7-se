#!/bin/bash

# DESCRIPTION
# -----------
# This installs additional features to be used by the Development Server
# Make sure to run install-lamp.sh prior to this.

# Update Status
tput setaf 4; tput bold; echo "Starting dev-lamp-dev-tools.sh"; tput sgr0

# SASS
tput setaf 6; tput bold; echo "SASS"; tput sgr0

# - Install SASS
tput setaf 2; tput bold; echo "Installing SASS"; tput sgr0
gem install sass

# NodeJS
tput setaf 6; tput bold; echo "NodeJS (needed for NPM)"; tput sgr0

# - Install NodeJS, needed for npm (Node Project Manager)
tput setaf 2; tput bold; echo "Installing NodeJS"; tput sgr0
apt-get -y install nodejs
# Make it compatible with older configurations, @see http://stackoverflow.com/questions/21491996
ln -s /usr/bin/nodejs /usr/bin/node

# NPM
tput setaf 6; tput bold; echo "NPM (needed for Bower)"; tput sgr0

# - Install npm (Node Project Manager), needed for Bower
tput setaf 2; tput bold; echo "Installing npm"; tput sgr0
apt-get -y install npm

# Bower
tput setaf 6; tput bold; echo "Bower"; tput sgr0

# - Install Bower, needed to manage theme libraries, such as Bourbon, jQuery, etc...
tput setaf 2; tput bold; echo "Installing Bower"; tput sgr0
npm install -g bower

# Unzip
tput setaf 6; tput bold; echo "Unzip"; tput sgr0

# - Install Unzip, required by Drush to install dependancy libraries
tput setaf 2; tput bold; echo "Installing Unzip"; tput sgr0
apt-get install unzip -y

# Finalizing
tput setaf 6; tput bold; echo "Finalizing"; tput sgr0

# - Restart Apache2 to take effect
tput setaf 2; tput bold; echo "Restarting Apache2"; tput sgr0
service apache2 restart
