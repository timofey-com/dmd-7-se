#!/bin/bash

# DESCRIPTION:
# Updates the Drupal instance with a website from the given repo.

# ARGUMENTS API
# $1 - Root of Drupal installation
# $2 - URL of repo

tput setaf 4; tput bold; echo "Starting repo-update.sh $1 $2"; tput sgr0

# Define variables
# - Get Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
# - Get website root directory
web_root="$1"
# - Define repo
repo="$2"

# Make & Define a temporary directory
git_dir=$(mktemp -d)

# Download git into a temporary directory
git clone $repo $git_dir
# Record git status. 0 = Success, else = Fail.
git_status=$?

# If download is Successful, update site
if test "$git_status" -eq 0; then

    # Get current theme
    cd $web_root
    web_theme=$(drush vget theme_default --exact)

    # Define Maintanance theme
    cd $web_root
    maintanance_theme="bartik"

    # Put site in maintanance mode
    cd $web_root
    drush vset maintenance_mode 1
    # switch theme to core
    drush vset theme_default $maintanance_theme

    # Clear cache.
    # This prevents site from serving cache with missing images. Instead a maintenance page will be shown.
    cd $web_root
    drush cc all

    # Update site
    rm $web_root/sites/all -rf
    mv $git_dir/sites/all/ $web_root/sites/all
    rm $git_dir -rf

    # Fix permissions
    $current_dir/fix-drupal-permissions.sh "$web_root"

    # Turn off maintanance mode
    cd $web_root
    drush vset maintenance_mode 0
    # switch theme back to web
    drush vset theme_default $web_theme

    # Update site
    drush up -y

    # Clear cache
    cd $web_root
    drush cc all

fi