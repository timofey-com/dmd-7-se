#!/bin/bash

# README
# Adds a domain to Virtual Host
# @see https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts
# @see https://www.digitalocean.com/community/tutorials/how-to-install-drupal-on-an-ubuntu-14-04-server-with-apache

# API
# $1 - Name of host (i.e. domain) to add
# $2 - Email of ServerAdmin
# $3 - Name of public_html directory (Optional. Defaults to 'public_html'.)

#Update Status
echo "Starting add-host.sh $1 $2 $3"

# Require root
if [ "$EUID" -ne 0 ]
  then tput setaf 1; echo "Please run as root, i.e. sudo ..."; tput sgr0
  exit
fi

# Load Variables
host_name=$1
serveradmin_email=$2
public_html=$3

# Process Variables
if [ -z "$public_html" ]; then
    public_html="public_html"
fi

echo "public_html=" "$public_html"

# Create a domain-specific Virtual Host
# - Make sure virtual host settings do not exist for this domain and it's not 'default' (12.04) or '000-default' (14.04)
if [ ! -f /etc/apache2/sites-available/${host_name}.conf ] || [ $host_name != 'default' ] || [ $host_name != '000-default' ]; then
    mkdir -p /var/www/$host_name/$public_html
    mkdir -p /var/www/$host_name/logs
    echo "Updating Apache2 VirtualHost Settings"
    echo "# @see
# https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts
# https://www.digitalocean.com/community/tutorials/how-to-install-drupal-on-an-ubuntu-14-04-server-with-apache
<VirtualHost *:80>
  ServerAdmin $serveradmin_email
  ServerName $host_name
  ServerAlias www.$host_name
  DocumentRoot /var/www/$host_name/$public_html
  <Directory /var/www/$host_name/$public_html>
    AllowOverride All
  </Directory>
  ErrorLog /var/www/$host_name/logs/error.log
  CustomLog /var/www/$host_name/logs/access.log combined
</VirtualHost>" > /etc/apache2/sites-available/$host_name.conf
    echo "Website root has been placed on /var/www/$host_name/$public_html/"
    echo "Done"

    # Enable the site/host
    a2ensite ${host_name}.conf

    # Reload Apache2
    service apache2 reload

    # Fix Permissions
    # @see http://askubuntu.com/questions/19898
    # - Add current user to www-data group
    # adduser $USER www-data
    chown -R www-data:www-data /var/www/*
    chmod -R g+rw /var/www/*
    find /var/www/* -type d -print0 | xargs -0 chmod g+s
else
    echo "Error! Domain already exists in settings. Remove /etc/apache2/sites-available/${host_name}.conf and retry."
fi