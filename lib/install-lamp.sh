#!/bin/bash

# DESCRIPTION
# This file installs LAMP

# ARGUMENTS API
# $1 - Email of ServerAdmin
# $2 - MYSQL root password

# Update Status
tput setaf 4; tput bold; echo "Starting install-lamp.sh $1 $2"; tput sgr0

# Process arguments
serveradmin_email="$1"
mysql_pass="$2"

# Initializing
tput setaf 6; tput bold; echo "Initializing"; tput sgr0

# - Updatwe Package List
tput setaf 2; tput bold; echo "Updating Package List"; tput sgr0
apt-get update

# Apache2
tput setaf 6; tput bold; echo "Apache2"; tput sgr0

# Install Apache 2
tput setaf 2; tput bold; echo "Installing Apache2"; tput sgr0
apt-get -y install apache2

# - Create the default Virtual Host
# - To add a domain, see add-host.sh.example or duplicate these settings and replace 'default' with domain name.
rm -rf /var/www/html
mkdir -p /var/www/default/public_html
tput setaf 2; tput bold; echo "Updating Apache2 VirtualHost Settings"; tput sgr0
echo "# @see
# https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts
# https://www.digitalocean.com/community/tutorials/how-to-install-drupal-on-an-ubuntu-14-04-server-with-apache
<VirtualHost *:80>
  ServerAdmin $serveradmin_email
  ServerName localhost
  DocumentRoot /var/www/default/public_html
  <Directory /var/www/default/public_html>
    AllowOverride All
  </Directory>
  ErrorLog /error.log
  CustomLog /access.log combined
</VirtualHost>" > /etc/apache2/sites-available/000-default.conf
tput setaf 2; tput bold; echo "Website root has been placed on /var/www/default/public_html/"; tput sgr0

# PHP5
tput setaf 6; tput bold; echo "PHP"; tput sgr0

# - Install PHP5
tput setaf 2; tput bold; echo "Installing PHP"; tput sgr0
apt-get -y install php5 libapache2-mod-php5 php5-mcrypt

# - Enable PHP Error Logging
tput setaf 2; tput bold; echo "Enabling Error Logging In PHP"; tput sgr0
mkdir /var/log/php5
touch /var/log/php5/error.log
# copy apache owner & permissions
chmod 750 /var/log/php5
chmod 640 /var/log/php5/error.log
chown -R www-data: /var/log/php5
cat <<EOF >> /etc/php5/apache2/php.ini

; Added by DMD 7 SE
; Log PHP errors to file
error_log = /var/log/php5/error.log
error_reporting = E_ALL
; Enable logging of PHP errors by default
log_errors = On
EOF
tput setaf 2; tput bold; echo "PHP logs are available on /var/log/php5/error.log"; tput sgr0

# - Secure PHP
tput setaf 2; tput bold; echo "Securing PHP"; tput sgr0
cat <<EOF >> /etc/php5/apache2/php.ini
; Hide PHP Details From Client
expose_php = Off
; Disable fopen. Better to avoid, but if must use, consider using curl.
allow_url_fopen = Off
EOF

# MYSQL
# Inspired by https://gist.github.com/reidcooper/8a9dc24667a313e236a6
tput setaf 6; tput bold; echo "MySQL"; tput sgr0

# - Install MYSQL
tput setaf 2; tput bold; echo "Install MySQL"; tput sgr0
echo "mysql-server mysql-server/root_password password $mysql_pass" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $mysql_pass" | debconf-set-selections
apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql

# - Initialize MYSQL
tput setaf 2; tput bold; echo "Initializing MYSQL"; tput sgr0
mysql_install_db

# - Secure MYSQL
# -- Based on https://gist.github.com/Mins/4602864
tput setaf 2; tput bold; echo "Securing MYSQL"; tput sgr0

# -- Temporarily install expect, to automate the next process
tput setaf 2; tput bold; echo "Installing expect (needed to automate the next process)"; tput sgr0
apt-get -y install expect

# -- Continue securing MYSQL
tput setaf 2; tput bold; echo "Continue securing MYSQL"; tput sgr0

SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"$mysql_pass\r\"
expect \"Change the root password?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")

echo "$SECURE_MYSQL"

# -- Uninstall expect, since it was a temporary thing
tput setaf 2; tput bold; echo "Uninstalling expect, since it was temporary"; tput sgr0
apt-get -y purge expect


# - Adjust MYSQL memory settings
cat <<EOF >> /etc/mysql/my.cnf

# Added by DMD 7 SE
# Increase memory to prevent Drupal from crashing
max_allowed_packet = 64M
key_buffer = 128M
EOF

# Finalizing
tput setaf 6; tput bold; echo "Finalizing"; tput sgr0

# Update Permissions
# @see http://askubuntu.com/questions/19898
# - Add current user to www-data group
# adduser $USER www-data
tput setaf 2; tput bold; echo "Updating Permissions"; tput sgr0
chown -R www-data:www-data /var/www/*
chmod -R g+rw /var/www/*
find /var/www/* -type d -print0 | xargs -0 chmod g+s

# - Restart Apache2 to take effect
tput setaf 2; tput bold; echo "Restarting Apache2"; tput sgr0
service apache2 restart
