#!/bin/bash

# DESCRIPTION
# This file installs LAMP Essentials, including GIT & Drush

# Update Status
tput setaf 4; tput bold; echo "Starting install-lamp-essentials.sh"; tput sgr0

# GIT
tput setaf 6; tput bold; echo "GIT"; tput sgr0

# - Install Git (also needed to install Drush)
tput setaf 2; tput bold; echo "Installing GIT"; tput sgr0
apt-get install git -y

# DRUPAL-SPECIFIC
tput setaf 6; tput bold; echo "Drupal Specific"; tput sgr0

# - Install/Configure PHP components
tput setaf 2; tput bold; echo "Installing PHP5-GD"; tput sgr0
apt-get -y install php5-gd

tput setaf 2; tput bold; echo "Installing mod_rewrite"; tput sgr0
a2enmod rewrite

# - Install Composer
tput setaf 2; tput bold; echo "Installing Composer"; tput sgr0
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
ln -s /usr/local/bin/composer /usr/bin/composer
export PATH="$HOME/.composer/vendor/bin:$PATH"

#tput setaf 2; tput bold; echo "Installing Drush"; tput sgr0
#git clone https://github.com/drush-ops/drush.git /usr/local/src/drush
#cd /usr/local/src/drush
#git checkout master
#ln -s /usr/local/src/drush/drush /usr/bin/drush
#composer install

# Finalizing
tput setaf 6; tput bold; echo "Finalizing"; tput sgr0

# - Restart Apache2 to take effect
tput setaf 2; tput bold; echo "Restarting Apache2"; tput sgr0
service apache2 restart
