#!/bin/bash

# DESCRIPTION
# Fix Drupal Permissions

# ARGUMENTS API
# $1 - Web directory root

# Update Status
tput setaf 4; tput bold; echo "Starting fix-drupal-permissions.sh $1 $2"; tput sgr0

# Process arguments
web_root="$1"

# Fix permissions
cd "$web_root"
# Fix file ownership
chown -R www-data:www-data .
# Chmod everything to 644
chmod 644 -R .
# Chmod directories to 755
find . -type d -exec chmod 755 {} +
# Chmod upload directory
# - The 2*** makes all new files inherit the www-data group. The **7* gives the group permission to write files
chmod 2775 ./sites/default/files
# Chmod settings files to 444
chmod 444 ./sites/default/settings.php
chmod 444 ./sites/default/default.settings.php
