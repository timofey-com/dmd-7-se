#!/bin/bash

# DESCRIPTION
# This installs a new installation of Drupal

# Update Status
tput setaf 4; tput bold; echo "Starting install-fail2ban.sh"; tput sgr0

apt-get -y install fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sed -i "s/action = %(action_)s/action = %(action_mwl)s/g" /etc/fail2ban/jail.local
sed -i "/^\[ssh-ddos\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[apache\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[apache-noscript\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[apache-overflows\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[php-url-fopen\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[php-url-fopen\]$/,/^\[/s/logpath *= .*/logpath = \/var\/log\/php*\/error.log/g" /etc/fail2ban/jail.local
sed -i "/^\[mysqld-auth\]$/,/^\[/s/enabled *= .*/enabled = true/g" /etc/fail2ban/jail.local
sed -i "/^\[mysqld-auth\]$/,/^\[/s/logpath *= .*/logpath = \/var\/log\/mysql\/error.log/g" /etc/fail2ban/jail.local

service fail2ban restart