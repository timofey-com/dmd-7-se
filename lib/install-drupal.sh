#!/bin/bash

# DESCRIPTION
# This installs a new installation of Drupal

# ARGUMENTS API
# $1 - Root of Drupal installation
# $2 - Minimal Installation, i.e. y
# $3 - MYSQL username, i.e. root
# $4 - MYSQL schema (db name), preferably limit to characters, do not use the name "default", limit to 64 characters
# $5 - MYSQL password
# $6 - Drupal admin username, i.e. root
# $7 - Drupal admin password
# $8 - Drupal admin email

# Require root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, i.e. sudo ..."
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting install-drupal.sh $1 $2 $3 $4 $5 $6 $7 $8"; tput sgr0

# Define variables
# - Get Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
web_root="$1"
site_ismin="$2"
mysql_user="$3"
mysql_schema="$4"
mysql_pass="$5"
drupal_admin_user="$6"
drupal_admin_pass="$7"
drupal_admin_email="$8"

# Download Drupal
echo "Downloading Drupal..."
mkdir -p $web_root
cd $web_root
drush dl drupal-7
# - Rename Drupal directory
shopt -s dotglob
mv drupal-7.*/* .
rm -r drupal-7.*

# Install Drupal instance
tput setaf 2; tput bold; echo "Installing Database"; tput sgr0

# Install Drupal
if [[ $site_ismin =~ ^(Y|y|yes)$ ]]
then
drush si minimal --db-url=mysql://$mysql_user:$mysql_pass@localhost/$mysql_schema --account-name=$drupal_admin_user --account-pass=$drupal_admin_pass --account-mail=$drupal_admin_email --site-name="Minimal Installation" -y
else
drush si --db-url=mysql://$mysql_user:$mysql_pass@localhost/$mysql_schema --account-name=$drupal_admin_user --account-pass=$drupal_admin_pass --account-mail=$drupal_admin_email.com --site-name="Standard Installation" -y
fi

# Set admin theme
cd $web_root
drush vset admin_theme seven
drush vset node_admin_theme 1

# Fix permissions
$current_dir/fix-drupal-permissions.sh "$web_root"

# Finish
echo "A new Drupal instance has been added to $web_root"