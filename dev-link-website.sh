#!/bin/bash

# DESCRIPTION
# This file links an existing website

# ARGUMENTS API
# $1 - Location of the new website's /sites/all directory, under vagrant, i.e. /public_html/sites/all

# Require root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, i.e. sudo ..."
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting dev-link-website.sh"; tput sgr0

# Define variables
# - Get Current Dir
current_dir="$(dirname "$(which "$0")")"
web_root="/var/www/default/public_html"

# Process arguments
web_dir="$1"

# Remove old site
tput setaf 2; tput bold; echo "Remove old site"; tput sgr0
rm -rf /var/www/default/public_html/sites/all

# Link new site
tput setaf 2; tput bold; echo "Link new site"; tput sgr0
ln -s /vagrant$web_dir $web_root/sites/all

# Enable custom_core (important for new installations)
tput setaf 2; tput bold; echo "Enabling custom_core module"; tput sgr0
cd $web_root
drush en -y custom_core

# Fix permissions
$current_dir/lib/fix-drupal-permissions.sh "$web_root"