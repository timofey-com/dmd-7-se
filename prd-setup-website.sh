#!/bin/bash

# DESCRIPTION
# Installs a website from repo with a fresh Drupal core. Adds a script to pull in updates.

# ARGUMENTS API
# $1 - URL of the REPO
# $2 - Domain name (host)
# $3 - Email of Server Admin
# $4 - Drupal admin username, i.e. root
# $5 - Drupal admin password
# $6 - Drupal admin email
# $7 - MYSQL schema (db name), preferably limit to characters, do not use the name "default", limit to 64 characters
# $8 - MYSQL username, i.e. root
# $9 - MYSQL password
# ${10} - Minimal Installation, i.e. y

# Require root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, i.e. sudo ..."
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting prd-setup-website.sh $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10}"; tput sgr0

# Define variables
# - Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
repo="$1"
host_name="$2"
serveradmin_email="$3"
drupal_admin_user="$4"
drupal_admin_pass="$5"
drupal_admin_email="$6"
mysql_schema="$7"
mysql_user="$8"
mysql_pass="$9"
site_ismin="${10}"

# Setup
if [ -z "$repo" ]; then
    read -e -p "Enter repo of your website: " repo
fi

if [ -z "$host_name" ]; then
    read -e -p "What domain would you like host? If not using domain, enter \"default\": " -i "default" host_name
fi

# Define Website directory
web_root="/var/www/$host_name"

if [ ! -f /etc/apache2/sites-available/${host_name}.conf ] && [ $host_name != 'default' ]; then
    # Get email of ServerAdmin
    if [ -z "$2" ]; then
        read -e -p "What is the email of the ServerAdmin, i.e. webmaster: " serveradmin_email
    fi
    # Create settings
    $current_dir/lib/add-host.sh $host_name $serveradmin_email
fi

if [ -z "$drupal_admin_user" ]; then
    read -e -p "Enter Drupal admin username: " -i "root" drupal_admin_user
fi

if [ -z "$drupal_admin_pass" ]; then
    read -e -p "Enter Drupal admin password: " drupal_admin_pass
fi

if [ -z "$drupal_admin_email" ]; then
    read -e -p "Enter Drupal admin email: " drupal_admin_email
fi

if [ -z "$mysql_schema" ]; then
    # Recommend db name based on host_name
    mysql_schema_default="drupal_${host_name//[^[:alnum:]]/}"
    read -e -p "Enter mysql schema (db name). Do not use the name 'default'. Limit to 64 characters: " -i "$mysql_schema_default" mysql_schema
fi

if [ -z "$mysql_user" ]; then
    read -e -p "Enter mysql username: " -i "root" mysql_user
fi

if [ -z "$mysql_pass" ]; then
    read -e -p "Enter mysql password: " mysql_pass
fi

if [ -z "$site_ismin" ]; then
    read -e -p "Minimal installation? (Y/n): " -i "Y" site_ismin
fi

# Install Drupal
$current_dir/lib/install-drupal.sh "$web_root/public_html" "$site_ismin" "$mysql_user" "$mysql_schema" "$mysql_pass" "$drupal_admin_user" "$drupal_admin_pass" "$drupal_admin_email"

# Load repo
$current_dir/lib/repo-update.sh "$web_root/public_html" $repo

# Enable custom_core (important for new installations)
cd "$web_root/public_html"
drush en -y custom_core

# Create a script to easily update the site in the future
mkdir -p $web_root/lib/contributed
cp -r $current_dir $web_root/lib/contributed/dmd-7-se

echo "#!/bin/bash

# Updates the website from the repo below
$web_root/lib/contributed/dmd-7-se/lib/repo-update.sh $web_root/public_html $repo
" > /var/www/$host_name/update.sh
chmod 744 /var/www/$host_name/update.sh
chmod 744 -R /var/www/$host_name/lib/contributed/dmd-7-se

echo "To update the site, commit your changes to $repo and simply execute /var/www/$host_name/update.sh"