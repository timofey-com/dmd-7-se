#!/bin/bash

# DESCRIPTION
# Creates a new Drupal website

# ARGUMENTS API
# $1 - Drupal admin username, i.e. root
# $2 - Drupal admin password
# $3 - Drupal admin email
# $4 - MYSQL schema (db name), preferably limit to characters, do not use the name "default", limit to 64 characters
# $5 - MYSQL username, i.e. root
# $6 - MYSQL password
# $7 - Minimal Installation, i.e. y

# Require root
if [ "$EUID" -ne 0 ]
  then tput setaf 1; echo "Please run as root, i.e. sudo ..."; tput sgr0
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting dev-setup-drupal.sh $1 $2 $3 $4 $5 $6 $7"; tput sgr0

# Define variables
# - Website directory
web_root="/var/www/default"
# - Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
drupal_admin_user="$1"
drupal_admin_pass="$2"
drupal_admin_email="$3"
mysql_schema="$4"
mysql_user="$5"
mysql_pass="$6"
site_ismin="$7"

# Setup
if [ -z "$drupal_admin_user" ]; then
    read -e -p "Enter Drupal admin username: " -i "root" drupal_admin_user
fi

if [ -z "$drupal_admin_pass" ]; then
    read -e -p "Enter Drupal admin password: " drupal_admin_pass
fi

if [ -z "$drupal_admin_email" ]; then
    read -e -p "Enter Drupal admin email: " drupal_admin_email
fi

if [ -z "$mysql_schema" ]; then
    # Recommend db name based on domain name
    parent_pwd=$(dirname "$web_root/public_html")
    parent_dir="${parent_pwd##*/}"
    parent_dir_clean="drupal_${parent_dir//[^[:alnum:]]/}"
    read -e -p "Enter mysql schema (db name). Do not use the name 'default'. Limit to 64 characters: " -i "$parent_dir_clean" mysql_schema
fi

if [ -z "$mysql_user" ]; then
    read -e -p "Enter mysql username: " -i "root" mysql_user
fi

if [ -z "$mysql_pass" ]; then
    read -e -p "Enter mysql password: " mysql_pass
fi

if [ -z "$site_ismin" ]; then
    read -e -p "Minimal installation? (Y/n): " -i "Y" site_ismin
fi

# Install Drupal
$current_dir/lib/install-drupal.sh "$web_root/public_html" "$site_ismin" "$mysql_user" "$mysql_schema" "$mysql_pass" "$drupal_admin_user" "$drupal_admin_pass" "$drupal_admin_email"