#!/bin/bash

# DESCRIPTION
# This file creates a new website template in an existing Drupal installation, based on DMD 7

# Require root
if [ "$EUID" -ne 0 ]
  then tput setaf 1; echo "Please run as root, i.e. sudo ..."; tput sgr0
  exit
fi

# Update Status
tput setaf 4; tput bold; echo "Starting dev-new-website.sh"; tput sgr0

# Define variables
# - Current Dir
current_dir="$(dirname "$(which "$0")")"

# Process arguments
web_dir="$1"

# Load DMD 7
tput setaf 2; tput bold; echo "Loading DMD 7"; tput sgr0
$current_dir/lib/repo-update.sh "$web_dir" "https://drtimofey@bitbucket.org/timofey-com/dmd-7.git"

# Prepare a new template

# Install Bourbon Front-end Framework
# @see bourbon.io
# Using sudo with bower due to permission issues.
tput setaf 2; tput bold; echo "Installing Bourbon framework"; tput sgr0
cd "$web_dir"/sites/all/themes/default1/lib/contributed
sudo bower install bourbon --allow-root --config.interactive=false
sudo bower install neat --allow-root --config.interactive=false
# CSS base settings (i.e. Bourbon Bitters) can be configured at sites/all/themes/default1/.scss/base
cd "$web_dir"/sites/all/themes/default1/.scss
gem install bitters
bitters install