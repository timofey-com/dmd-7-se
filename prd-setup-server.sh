#!/bin/bash

# Require root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, i.e. sudo ..."
  exit
fi

# ARGUMENTS API
# -------------
# $1 - Email of ServerAdmin (Optional)
# $2 - MYSQL root password (Optional)

# Define variables
# Get Current Dir
current_dir="$(dirname "$(which "$0")")"

# Update Status
tput setaf 4; tput bold; echo "Starting prd-setup-server.sh $1 $2"; tput sgr0

# Process arguments
if [ -z "$1" ]; then
    read -e -p "What is the email of the ServerAdmin: " serveradmin_email
else
    serveradmin_email="$1"
fi

if [ -z "$2" ]; then
    read -e -p "Set your MYSQL root password (don't forget to write down!): " mysql_pass
else
    mysql_pass="$2"
fi

# Install LAMP
$current_dir/lib/install-lamp.sh "$serveradmin_email" "$mysql_pass"

# Install LAMP Essentials
$current_dir/lib/install-lamp-essentials.sh

# Install fail2ban (security)
$current_dir/lib/install-fail2ban.sh